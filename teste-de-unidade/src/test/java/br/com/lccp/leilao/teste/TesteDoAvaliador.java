package br.com.lccp.leilao.teste;

import org.junit.Test;

import br.com.lccp.leilao.dominio.Lance;
import br.com.lccp.leilao.dominio.Leilao;
import br.com.lccp.leilao.dominio.Usuario;
import br.com.lccp.leilao.servico.Avaliador;
import org.junit.Assert;

public class TesteDoAvaliador {
	
	@Test
	public void deveEntenderLancesEmOrdemCrescente() {
		Usuario joao = new Usuario("joao");
		Usuario jose = new Usuario("jose");
		Usuario maria = new Usuario("maria");
		
		Leilao leilao = new Leilao("Playstation 4 Novo");
		
		leilao.propoe(new Lance(joao, 250.0));
		leilao.propoe(new Lance(jose, 300.0));
		leilao.propoe(new Lance(maria, 400.0));
		
		
		Avaliador leiloeiro = new Avaliador();
		
		leiloeiro.avalia(leilao);
		
		
		double maiorEsperado = 400;
		double menorEsperado = 250;
		
		
		Assert.assertEquals(maiorEsperado, leiloeiro.getMaiorLance(),0.0001);
		
		Assert.assertEquals(menorEsperado, leiloeiro.getMenorLance(),0.0001);
	}
	
	@Test
	public void deveEntenderUmUnicoLance() {
		Usuario joao = new Usuario("joao");
		
		Leilao leilao = new Leilao("Playstation 4 Novo");
		
		leilao.propoe(new Lance(joao, 250.0));
		
		Avaliador leiloeiro = new Avaliador();
		
		leiloeiro.avalia(leilao);
		
		
		double maiorEsperado = 250;
		double menorEsperado = 250;
		
		
		Assert.assertEquals(maiorEsperado, leiloeiro.getMaiorLance(),0.0001);
		
		Assert.assertEquals(menorEsperado, leiloeiro.getMenorLance(),0.0001);
	}
	
	@Test
	public void deveEntenderLancesEmOrdemAleatoria() {
		Usuario joao = new Usuario("joao");
		
		Leilao leilao = new Leilao("Playstation 4 Novo");
		
		leilao.propoe(new Lance(joao, 200.0));
		leilao.propoe(new Lance(joao, 450.0));	
		leilao.propoe(new Lance(joao, 120.0));
		leilao.propoe(new Lance(joao, 700.0));
		leilao.propoe(new Lance(joao, 630.0));
		leilao.propoe(new Lance(joao, 230.0));
		
		Avaliador leiloeiro = new Avaliador();
		
		leiloeiro.avalia(leilao);
		
		
		double maiorEsperado = 700;
		double menorEsperado = 120;
		
		
		Assert.assertEquals(maiorEsperado, leiloeiro.getMaiorLance(),0.0001);
		
		Assert.assertEquals(menorEsperado, leiloeiro.getMenorLance(),0.0001);
	}
	
	@Test
	public void deveEntenderLancesEmOrdemDecrescente() {
		Usuario joao = new Usuario("joao");
		
		Leilao leilao = new Leilao("Playstation 4 Novo");
		
		leilao.propoe(new Lance(joao, 400.0));
		leilao.propoe(new Lance(joao, 300.0));	
		leilao.propoe(new Lance(joao, 200.0));
		leilao.propoe(new Lance(joao, 100.0));
		
		Avaliador leiloeiro = new Avaliador();
		
		leiloeiro.avalia(leilao);
		
		Assert.assertEquals(400, leiloeiro.getMaiorLance(),0.0001);
		
		Assert.assertEquals(100, leiloeiro.getMenorLance(),0.0001);
	}

}
